import json
import socket
import time
import sys
from urllib.request import urlopen
import requests
import re

#------------------------------------------

banner = """                                                                                                         
  ,---,                                                                   .---.            ,--,            
,--.' |                                                                  /. ./|          ,--.'|     .--.,  
|  |  :             ,--,      ,---,              __  ,-.             .--'.  ' ;   ,---.  |  | :   ,--.'  \ 
:  :  :           ,'_ /|  ,-+-. /  |  ,----._,.,' ,'/ /|            /__./ \ : |  '   ,'\ :  : '   |  | /\/ 
:  |  |,--.  .--. |  | : ,--.'|'   | /   /  ' /'  | |' |   .--, .--'.  '   \' . /   /   ||  ' |   :  : :   
|  :  '   |,'_ /| :  . ||   |  ,"' ||   :     ||  |   ,' /_ ./|/___/ \ |    ' '.   ; ,. :'  | |   :  | |-, 
|  |   /' :|  ' | |  . .|   | /  | ||   | .\  .'  :  /, ' , ' :;   \  \;      :'   | |: :|  | :   |  : :/| 
'  :  | | ||  | ' |  | ||   | |  | |.   ; ';  ||  | '/___/ \: | \   ;  `      |'   | .; :'  : |__ |  |  .' 
|  |  ' | ::  | : ;  ; ||   | |  |/ '   .   . |;  : | .  \  ' |  .   \    .\  ;|   :    ||  | '.'|'  : '   
|  :  :_:,''  :  `--'   \   | |--'   `---`-'| ||  , ;  \  ;   :   \   \   ' \ | \   \  / ;  :    ;|  | |   
|  | ,'    :  ,      .-./   |/       .'__/\_: | ---'    \  \  ;    :   '  |--"   `----'  |  ,   / |  : \   
`--''       `--`----'   '---'        |   :    :          :  \  \    \   \ ;               ---`-'  |  |,'   
                                      \   \  /            \  ' ;     '---"                        `--'     
                                       `--`-'              `--`                                                  Version 0.0.1

                                                                                                               """

__author__ = 'Recep Kalmac'
__copyrigth__ = 'Copyright 2018, hungryWolf Project'

#-------------------------------------------
def fetch(url,decoding = 'utf-8'):
    return urlopen(url).read().decode(decoding)

def heading(head,website,afterWebHead):
    var = str(head + " '" + website + "'" + str(afterWebHead))
    length = len(var) + 1
    print("")
    print(var+"\n")
    print("length : " , length)
    print("")
    print("length : " , length)


# this _headers can bu updated
_headers = {

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',  # updating user-agent
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': 'gzip,deflate,sdch',
    'Accept-Language': 'en-US,en;q=0.8',
    'Connection': 'keep-alive'

}
# information , for wrong entered website info

empty_website = "[-]\tNot A Website Entered,Please Enter A Website\n"
wrong_URL = "[-]!!! Wrong URL Is Detected \tPlease Enter A Valid And Correct URL\n(i.e, facebook.com\tgoogle.com)"
str_Index = "[-]\t Input An Integer Please\t\t(i.e 1,2,3,4,5,6)"
value_Select = "[-]\tPlease Use The Index Value From The List\n\t\t[!!]Not yours..."

def isWebSiteEmpty(website):
    if website != None:
        return "Website is valid"
    else:
        return "Website is not valid"
def validWebsiteURL(website):
    web = isWebSiteEmpty(website)
    if web is "valid":
        if not (re.match(r"(^(http://|https://)?([a-z0-9][a-z0-9-]*\.)+[a-z0-9][a-z0-9-]*$)",website)):
            exit(wrong_URL)
        else:
            exit(empty_website)

# the aim of is that reducing wrong url
def replaceURLpath(website):

    web = isWebSiteEmpty(website)   # check is valid or not
    web = web.replace("http://","")
    web = web.replace("htt://","")
    web = web.replace("htp://","")
    web = web.replace("http://www.","")
    web = web.replace("https://","")
    web = web.replace("https://www.","")
    web = web.replace("www.","")
    return web

#removing HTTP Errors
def removeHTTP(website):
    website = replaceURLpath(website)
    return website

# add http:// beginning of the target website or domain name
def addHTTP(website):
    website = replaceURLpath(website)
    website = ("http://"+website)
    return website

def write(var,data):
    if len(var) == 0:
        print(str(data))
    elif len(var) != 0:
        print(var + str(data))
    else:
        pass
def Request(website , _timeout = None , _encode = None):
    try:
        if _encode == None:
            return requests.get(website, headers = _headers , timeout = _timeout).content
        elif _encode == True:
            return requests.get(website,headers = _headers,timeout = _timeout).text.encode('UTF-8')
        else :
            sys.exit(1)
            pass

    except Exception as e:
        print("[-]\tError is : %s \t" % e)
#-------------------------------------------

def cloudFlare_bypass(website):
    # website = removeHTTP(website)
    # website = addHTTP(website)

    # subdomain list , if you can find more subdomain list , you can add

    subdomain_list = ["ftp", "cpanel", "webmail", "localhost", "local", "mysql", "forum", "driect-connect", "blog",
                      "vb", "forums", "home", "direct", "forums", "mail", "access", "admin", "administrator",
                      "email", "downloads", "ssh", "owa", "bbs", "webmin", "paralel", "parallels", "www0", "www",
                      "www1", "www2", "www3", "www4", "www5", "shop", "api", "blogs", "test", "mx1", "cdn", "mysql",
                      "mail1", "secure", "server", "ns1", "ns2", "smtp", "vpn", "m", "mail2", "postal", "support",
                      "web", "dev", "api", "ic", "demo", "beta", "personel", "manager", "accounts", "cms"]

    for sublist in subdomain_list:
        try:
            if "http://" in website:
                website = website.replace("http://", "")
            elif "https://" in website:
                website = website.replace("https://", "")

            website = website.replace("www.", "")
            host = str(sublist) + "." + str(website)
            # print(host)
            show_IP = socket.gethostbyname(str(host))
            print("==============================================================")
            print("[!] CloudFlare Bypass --->  " + str(show_IP) + ' : ' + str(host))
            print("==============================================================")
        except:
            print("[!] No domains")
def dnslookup(website):

        if "https://" in website:
            website = website.replace("https://","")
        elif "http://" in website:
            website = website.replace("http://","")
        elif "www." in website:
            website = website.replace("www.","")

        #print("website ----- > " , website , "\n")
        url = "http://api.hackertarget.com/dnslookup/?q=" + website
        #print("url  ---------------------> " , url , "\n")

        f_url = "{url}{website}".format(url=url,website=website)
        #print("f_url - -----> " , f_url , "\n")
        #print("length f_url ------> " , len(f_url) , "\n")
        req = requests.get(f_url,headers=_headers,timeout = 5).text.encode('UTF-8')

        #print("req ---> " , req , "\n")
        #print("length req ---> ", len(req) , "\n")

        if not len(req) == 0 :
            a = req.decode('UTF-8')
            list = str(a).strip("").split("\n")
            for _link in list:
                if not len(_link) == None:
                    print("[+] ",_link)
                else:
                    print("[-] "," No domains...")
def findshareddns(website):
    if "https://" in website:
        website = website.replace("https://", "")
    elif "http://" in website:
        website = website.replace("http://", "")
    elif "www." in website:
        website = website.replace("www.", "")

    url = "http://api.hackertarget.com/findshareddns/?q=" + website
    combo = "{url}{website}".format(url=url, website=website)

    req = requests.get(combo, headers=_headers, timeout=5).text.encode('UTF-8')
    if len(req) != 0:
        a = req.decode('UTF-8')
        list = str(a).strip("").split("\n")
        for _link in list:
            if len(_link) != 0:
                print("[+] ", _link)
            else:
                print("[-] ", "No domains...")
def geo2ip(website):

    if "https://" in website:
        website = website.replace("https://", "")
    elif "http://" in website:
        website = website.replace("http://", "")
    elif "www." in website:
        website = website.replace("www.", "")

    url = "http://api.hackertarget.com/geoip/?q=" + website
    f_url = "{url}{website}".format(url=url, website=website)

    req = requests.get(f_url, headers=_headers, timeout=5).text.encode('UTF-8')
    if not len(req) == 0:
        a = req.decode('UTF-8')
        list = str(a).strip("").split("\n")
        for _link in list:
            if len(_link) != 0:
                print("[+]", _link)
            else:
                print("[-] ", "No domains...")
def get_domain(website):

    if "https://" in website:
        website = website.replace("https://", "")
    elif "http://" in website:
        website = website.replace("http://", "")
    elif "www." in website:
        website = website.replace("www.", "")

    url = "https://domains.yougetsignal.com/domains.php"

    post = {

        'remoteAddress' : website,
        'key'           : ''
    }

    req = requests.post(url,headers = _headers , timeout = 5 , data=post).text.encode('UTF-8')
    grab = json.loads(req)

    Status = grab['status']
    IP = grab['remoteIpAddress']
    Domain = grab['remoteAddress']
    Total_Domains = grab['domainCount']
    Array = grab['domainArray']

    if (Status == 'Fail'):
        print("[-] ","Reverse IP Limit Reached")
    else:
        print("[+] ","IP            : " + IP +"")
        print("[+] ","Domain        : " + Domain +"")
        print("[+] ","Total Domains : "+ Total_Domains + "")

        domains = []

        for x , y in Array:
            domains.append(x)
        for res in domains:
            print("+ " , res)
def port_checker(domain_address):
    try:
        url_path = "http://api.hackertarget.com/nmap/?q=" + domain_address
        fetch_url_path = fetch(url_path)
        print("RESULT:\n")
        print("==============================================================")
        print(fetch_url_path)
        print("==============================================================")
    except :
        print("[-] " , " Unreachable domain")
def reverse_IP_Lookup(website):

    if "https://" in website:
        website = website.replace("https://", "")
    elif "http://" in website:
        website = website.replace("http://", "")
    elif "www." in website:
        website = website.replace("www.", "")

    url = "http://api.hackertarget.com/reverseiplookup/?q=" + website
    combo = "{url}{website}".format(url=url, website=website)

    req = requests.get(combo, headers=_headers, timeout=5).text.encode('UTF-8')
    if len(req) != 0:
        a = req.decode('UTF-8')
        list = str(a).strip("").split("\n")
        for _link in list:
            if len(_link) != 0:
                print("[+] ", _link)
            else:
                print("[-] ", "No domains...")
def robotTxt(domain):
    if not (domain.startswith('http://') or domain.startswith('https://')):
        domain = 'http://' + domain
    robot = domain + '/robots.txt'
    try:

        probot = fetch(robot)
        print("RESULT:\n")
        print("==============================================================")
        print(probot)
        print("==============================================================")

    except Exception as e:
        print("[-] ", "no domains")
        print(e)
def traceroute(website):
    try:
        target = "https://api.hackertarget.com/mtr/?q=" + website
        t = fetch(target)
        print("RESULT:\n")
        print("==============================================================")
        print(t)
        print("==============================================================")
    except:
        print("[-]", "Unreachable...")

# todo : check again

def whois(website):
    if "https://" in website:
        website = website.replace("https://", "")
    elif "http://" in website:
        website = website.replace("http://", "")
    elif "www." in website:
        website = website.replace("www.", "")

    url = "http://api.hackertarget.com/whois/?q=" + website
    combo = "{url}{website}".format(url=url, website=website)

    req = requests.get(combo, headers=_headers, timeout=5).text.encode('UTF-8')
    if len(req) != 0:
        a = req.decode('UTF-8')
        list = str(a).strip("").split("\n")
        for _link in list:
            if len(_link) != 0:
                print("[+] ", _link)
            else:
                print("[-] ", "No domains...")

if __name__ == '__main__':
    
    print(banner)
    while True:
        print("1    - CloudFlareByPass\n"
              "2    - DnsLookUp\n"
              "3    - FindSharedDns\n"
              "4    - Geo2IP\n"
              "5    - GetDomainInformation\n"
              "6    - PortChecker\n"
              "7    - ReverseIPLookup\n"
              "8    - RobotTxt\n"
              "9    - Traceroute\n"
              "10   - Exit\n"
              "11   - Help\n"
              "12   - About"
              )

        choice = int(input("Enter \t: "))
        if choice == 1:
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            cloudFlare_bypass(url)
            print("\n")

        elif choice == 2:
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            dnslookup(url)
            print("\n")

        elif choice == 3:

            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            findshareddns(url)
            print("\n")

        elif choice == 4:
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            geo2ip(url)
            print("\n")

        elif choice == 5 :
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            get_domain(url)
            print("\n")

        elif choice == 6:
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            port_checker(url)
            print("\n")

        elif choice == 7 :
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            reverse_IP_Lookup(url)
            print("\n")

        elif choice == 8:
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            robotTxt(url)
            print("\n")

        elif choice == 9 :
            url = input("Enter target website : \t(i.e. facebook.com,google.com)\t")
            traceroute(url)
            print("\n")

        elif choice == 10:   #exit
            print("Good bye...")
            time.sleep(2)
            sys.exit(1)

        elif choice == 11:
            print("Just select one of them from the list , and enter the target domain address")
            print("\n")
        elif choice == 12:
            print("hungryWolf is written by Recep Kalmac")
        else:
            print("Invalid input !!!!")
            time.sleep(1)